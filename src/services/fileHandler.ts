import fs from "fs";
import path from "path";
import log from "electron-log";
import { app } from "electron";

export const usePDFFile = (buffer: any) => {
  const pdfPath = path.join(
    getPrintFolderPath(),
    "./" + randomString() + ".pdf"
  );
  log.info(pdfPath);
  fs.writeFileSync(pdfPath, buffer, "binary");
  log.info("after save");

  const removeFile = () => {
    try {
      fs.unlinkSync(pdfPath);
    } catch (error) {
      log.error("delete error" + error);
    }
  };
  return [pdfPath, removeFile];
};
export const checkAppFolder = () => {
  const folderPath = path.join(app.getPath("home"), "/.prints");
  log.info(folderPath);
  if (!fs.existsSync(folderPath)) fs.mkdir(folderPath, (e) => log.info(e));
};

export const getPrintFolderPath = () => {
  checkAppFolder();
  return path.join(app.getPath("home"), "/.prints");
};
function randomString() {
  return Math.random().toString(36).substring(7);
}

//add delete file
