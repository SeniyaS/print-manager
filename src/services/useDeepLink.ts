import { modes } from "./../configs/modes";
import env from "env";
import log, { info } from "electron-log";
import { AES, enc } from "crypto-js";
import { ErrorInfo } from "ts-loader/dist/interfaces";

export interface DeepLinkResult {
  mode: "dir_print" | "view" | "print" | "viewAndPrint";
  pdfUrl: string;
}
export const useDeepLink = (): DeepLinkResult => {
  try {
    log.info(process.argv.toString());

    if (!!!process.argv[1]) {
      log.info("no url");
      throw new Error("No Url Found");
    }
    const result: DeepLinkResult = readDeepLinkParameters(process.argv[1]);

    if (!!!result.pdfUrl) throw new Error("No PDF URL found");
    //TODO print mode validation
    // if (!!!result.mode) throw new Error("No Mode defined");

    // console.log("new link:::",AESEncrypt("pdf=https://reservemn.usedirect.com/MinnesotaWeb/themes/Minnesota/dummy.pdf&mode=dir_print","0772920387@ssmrs"));
    // const urlData: string = AESDecrypt(text, "0772920387@ssmrs");
    // console.log(urlData);
    // const para = decodeUrlParams(text);
    return result;
  } catch (error) {
    throw new Error(error.message);
  }
};

const AESEncrypt = (plainText: string, key: string): string => {
  const cyperedText = AES.encrypt(plainText, key);
  return cyperedText.toString();
};

export const AESDecrypt = (text: string, key: string): string => {
  const byte = AES.decrypt(text, key);
  log.info("AES decrypt", byte.toString(enc.Utf8));
  return byte.toString(enc.Utf8);
};

const readDeepLinkParameters = (deepLinkUrl: string) => {
  const url =
    env.name == "production" ? new URL(deepLinkUrl) : new URL(env.deepLink);

  const params = new URLSearchParams(url.search.slice(1));

  const result: DeepLinkResult = {
    mode: <"dir_print" | "view" | "print" | "viewAndPrint">params.get("mode"),
    pdfUrl: <string>decodeURI(params.get("msg") as string),
  };
  return result;
};
const decodeUrlParams = (text: string): string[] => {
  const textArr = text.split("&");
  return textArr.map((val, i) => {
    const str = val.split("=");
    return str.slice(1)[0];
  });
};
