import ptp from "pdf-to-printer";
import log from "electron-log";

export const printService = async (pdfPath: string) => {
  try {
    //prints using defualt printer
    return ptp.print(pdfPath).then((res) => {
      return res;
    });
  } catch (error) {
    log.error(error);
    log.info(error.message);
  }
};
export const printWithOptions = async (pdfPath: string, printer: string) => {
  try {
    //prints using defualt printer
    const res = await ptp.print(pdfPath, { printer });
    return res;
  } catch (error) {
    log.error(error);
    log.info(error.message);
  }
};
export const getPrintersList = async () => {
  try {
    const res = await ptp.getPrinters();
    return res;
  } catch (error) {}
};

export const getDefaultPrinter = async () => {
  try {
    const res = await ptp.getDefaultPrinter();
    return res;
  } catch (error) {}
};
