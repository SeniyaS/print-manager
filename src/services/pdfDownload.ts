import { usePDFFile } from "./fileHandler";
import fetch from "electron-fetch";
import log from "electron-log";
export const downloadPDF = (url: string): Promise<any> => {
  return fetch(url).then((res) =>
    res.arrayBuffer().then((buff) => {
      try {
        //synchronously saving PDF
        const [path, remove] = usePDFFile(Buffer.from(buff));
        log.info(path);
        return [path, remove];
      } catch (error) {
        log.error("save or print error" + error);
        throw new Error("Error Loading PDF");
      }
    })
  );
};
