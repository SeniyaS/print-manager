import { IPCError } from "./../helpers/models/models";
import "./stylesheets/main.css";
import "../helpers/context_menu.js";
import "../helpers/external_links.js";
import { IpcMain, IpcRendererEvent, remote } from "electron";
import jetpack from "fs-jetpack";
import { ipcRenderer } from "electron";
import { ipcTopics } from "../configs/ipcConfigs";
import { Modal } from "mdb-ui-kit";
const app = remote.app;
const appDir = jetpack.cwd(app.getAppPath());
// const manifest = appDir.read("package.json", "json");
const modalEl = document.getElementById("exampleModal");
const printModal = new Modal(modalEl);

const changePDF = (event: IpcRendererEvent, path: string) => {
  document.getElementById("viewer")!.setAttribute("src", path + "#toolbar=0");
};

const hideLoader = (event: IpcRendererEvent, isShow: boolean) => {
  document.getElementById("loader")!.hidden = isShow;
};

const addPrinterList = (event: IpcRendererEvent, list: string[]) => {
  console.log(list);
  list.forEach((p) => {
    const opt = document.createElement("option");
    opt.setAttribute("value", p);
    opt.innerHTML = p;
    document.getElementById("printerSelect")!.appendChild(opt);
  });
};

const hideNavPrintBtn = (event: IpcRendererEvent, isShow: boolean) => {
  document.getElementById("navPrintBtn")!.hidden = isShow;
};

const showPrintModal = (e: any, payload: any) => {
  printModal.show();
};

const setDefPrinter = (e: Event, printerName: string) => {
  document.getElementById("def-printer")!.innerText = printerName;
};

const hideErrorContainer = (e: Event, error: IPCError) => {
  const { isHideErrorContainer: isHide, message, title } = error;

  document.getElementById("errorContainer")!.hidden = isHide;
  if (isHide) document.getElementById("viewer")!.hidden = false;
  else {
    document.getElementById("viewer")!.hidden = true;
    document.getElementById("errorTitle")!.innerHTML = title.toUpperCase();
    document.getElementById("ErrorDescriptions")!.innerHTML = message;
  }
};
//--event listeners
document.getElementById("btnPrint")!.addEventListener("click", () => {
  const opt: any = document.getElementById("printerSelect");
  ipcRenderer.send(ipcTopics.PRINT_WITH_OPTIONS, opt!.value);
  // printModal.dispose();
  printModal.hide();
});

document.getElementById("btnPrintClose")!.addEventListener("click", () => {
  const w = remote.getCurrentWindow();
  w.close();
});

document.getElementById("navPrintBtn")!.addEventListener("click", () => {
  printModal.show();
});

//IPC listeners
ipcRenderer.on(ipcTopics.HIDE_NAV_PRINT_BTN, hideNavPrintBtn);
ipcRenderer.on(ipcTopics.GET_PRINTERS, addPrinterList);
ipcRenderer.on(ipcTopics.CHANGE_PDF_URI, changePDF);
ipcRenderer.on(ipcTopics.HIDE_LOADER, hideLoader);
ipcRenderer.on(ipcTopics.SHOW_PRINT_MODAL, showPrintModal);
ipcRenderer.on(ipcTopics.SET_DEF_PRINTER, setDefPrinter);
ipcRenderer.on(ipcTopics.HIDE_ERROR_CONTAINER, hideErrorContainer);
