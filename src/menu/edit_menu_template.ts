import { app, MenuItem } from "electron";

export const editMenuTemplate: any = {
  label: "File",
  submenu: [
    { type: "separator" },
    {
      label: "Quit",
      accelerator: "CmdOrCtrl+Q",
      click: () => {
        app.quit();
      },
    },
  ],
};
