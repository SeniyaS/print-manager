export const ipcTopics = {
  CHANGE_PDF_URI: "changePdfUri",
  HIDE_LOADER: "hideLoader",
  GET_PRINTERS: "getPrinters",
  PRINT_WITH_OPTIONS: "printWithOptions",
  HIDE_NAV_PRINT_BTN:"hideNavPrintBtn",
  SHOW_PRINT_MODAL:"showPrintModal",
  SET_DEF_PRINTER:"setDefPrinter",
  HIDE_ERROR_CONTAINER:"isHideErrorContainer"
};
