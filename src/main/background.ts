import { IPCError } from "./../helpers/models/models";
import { DeepLinkResult } from "./../services/useDeepLink";
import path from "path";
import url from "url";
import { app, Menu } from "electron";
import { devMenuTemplate } from "../menu/dev_menu_template";
import { editMenuTemplate } from "../menu/edit_menu_template";
import createWindow from "../helpers/window";
import env from "env";
import { useDeepLink } from "../services/useDeepLink";
import log from "electron-log";
import { ipcTopics } from "../configs/ipcConfigs";
import { checkAppFolder } from "../services/fileHandler";
import { modes } from "../configs/modes";
import { getDefaultPrinter } from "pdf-to-printer";
import { handleDirectPrint } from "../helpers/handleDirectPrint";
import { handlePrint } from "../helpers/handlePrint";
import { handleViewAndPrint } from "../helpers/handleViewnPrint";
import { handleView } from "../helpers/handleView";
import JasperConnection from "../helpers/JasperConnection";
import { assert } from "console";
import { encrypt, encryptCryptoJS, encryptNew } from "../helpers/Chiper";
import { JasperReport } from "../helpers/ReportInvoker";

app.setAsDefaultProtocolClient("hansen");

const setApplicationMenu = () => {
  const menus = [editMenuTemplate];
  if (env.name !== "production") {
    menus.push(devMenuTemplate);
  }
  Menu.setApplicationMenu(Menu.buildFromTemplate(menus));
};

// Save userData in separate folders for each environment.
// Thanks to this you can use production and development versions of the app
// on same machine like those are two separate apps.
if (env.name !== "production") {
  const userDataPath = app.getPath("userData");
  app.setPath("userData", `${userDataPath} (${env.name})`);
}

app.on("ready", () => {
  setApplicationMenu();
  const mainWindow = createWindow("main", {
    width: 1000,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      plugins: true,
    },
  });

  mainWindow.loadURL(
    url.format({
      pathname: path.join(__dirname, "app.html"),
      protocol: "file:",
      slashes: true,
    })
  );

  if (env.name === "development") {
    mainWindow.webContents.openDevTools();
  }

  mainWindow.webContents.on("did-finish-load", async () => {
    try {
      //check for pdf download location,if not create the folder
      checkAppFolder();
      mainWindow.webContents.send(ipcTopics.HIDE_LOADER, false);
      mainWindow.webContents.send(
        ipcTopics.SET_DEF_PRINTER,
        await getDefaultPrinter()
      );
      const { mode, pdfUrl }: DeepLinkResult = useDeepLink();
      log.info("else");
      switch (mode) {
        case "dir_print":
          handleDirectPrint(pdfUrl);
          break;
        case "view":
          handleView(pdfUrl);
          break;
        case "print":
          handlePrint(pdfUrl);
          break;
        case "viewAndPrint":
          handleViewAndPrint(pdfUrl);
        default:
          break;
      }

      // encrypt();
      // var enc = encryptNew(
      //   "112233445566778899AABBCCDDEEFF78659483256707862095732543544",
      //   "hex",
      //   "password12345678910111213141516kjhoihoiuhouyoiuhoih",
      //   "utf8"
      // );
      // log.info("enc ", enc);

      const text =
        "u=SURE|o=lofcfusion|r=EXT_LOFCFUSION_USER,ROLE_USER|exp=20160203094739+0530|dbHost1=172.1.18.202|dbHost2=172.1.18.204|dbPort=1528|dbUname=rep005|dbPwd=rep321|dbName=fusiondv";
      // encryptCryptoJS(text);
      JasperReport.test();
      mainWindow.webContents.send(ipcTopics.HIDE_LOADER, true);
    } catch (error) {
      log.info("global error net");
      log.error(error);
      mainWindow.webContents.send(ipcTopics.HIDE_LOADER, true);
      mainWindow.webContents.send(ipcTopics.HIDE_ERROR_CONTAINER, {
        isHideErrorContainer: false,
        message: error.message,
        title: error.name,
      } as IPCError);
    }
  });
});
app.on("window-all-closed", () => {
  app.quit();
});

function test() {
  const js: JasperConnection = new JasperConnection(true, "lofcfusion", "pdf");
  js.setLoginURL("192.186.23.235:8080");
  js.setReportURL("192.186.23.235:8080");

  log.info("Login URL:" + js.getLoginURL());
  log.info("Report URL:" + js.getReportURL());
}
