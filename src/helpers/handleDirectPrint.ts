import log from "electron-log";
import { downloadPDF } from "../services/pdfDownload";
import { printService } from "../services/printService.electron";

const { BrowserWindow } = require("electron");

export const handleDirectPrint = async (pdfUrl: string) => {
  const mainWin = BrowserWindow.getFocusedWindow();
  log.info("dir print");
  const [docUri, removeFile] = await downloadPDF(pdfUrl);
  const res = await printService(docUri).then((res) => {
    removeFile();
  });

  log.info("removing file");

  log.info(res);
  //closing the window after 2sec, sending to print
  setTimeout(() => {
    mainWin.close();
  }, 2000);
};
