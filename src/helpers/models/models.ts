export interface IPCError {
  isHideErrorContainer: boolean;
  message: string;
  title: string;
}

export interface DeepLinkData {
  reportId: string;
  userId: string;
  mode: string;
  serverIp1: string;
  serverIp2: string;
  port: string;
  dbName: string;
  orgId: string;
  dbUName: string;
  dbPassword: string;
  IsCommercial: boolean;
}
