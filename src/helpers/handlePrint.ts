import { BrowserWindow, ipcMain, IpcMainEvent } from "electron";
import log from "electron-log";
import { ipcTopics } from "../configs/ipcConfigs";

const { downloadPDF } = require("../services/pdfDownload");
const {
  printWithOptions,
  getPrintersList,
} = require("../services/printService.electron");

export const handlePrint = async (pdfUrl: string) => {
  const mainWindow = BrowserWindow.getFocusedWindow();
  const printWOpt = async (e: IpcMainEvent, printer: string) => {
    const [doc, removeFile] = await downloadPDF(pdfUrl);
    log.info("printWOpt", printer);
    printWithOptions(doc, printer).then((res: any) => {
      removeFile();
      mainWindow?.close();
    });
  };

  ipcMain.on(ipcTopics.PRINT_WITH_OPTIONS, printWOpt);

  mainWindow!.webContents.send(ipcTopics.GET_PRINTERS, await getPrintersList());
  mainWindow!.webContents.send(ipcTopics.SHOW_PRINT_MODAL, null);
};
