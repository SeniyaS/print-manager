import { BrowserWindow } from "electron";
import { ipcTopics } from "../configs/ipcConfigs";
import { downloadPDF } from "../services/pdfDownload";

export const handleView = async (pdfUrl: string) => {
  const [doc, removeFile] = await downloadPDF(pdfUrl);
  const mainWindow = BrowserWindow.getFocusedWindow();

  mainWindow!.webContents.send(ipcTopics.CHANGE_PDF_URI, doc);
  mainWindow!.webContents.send(ipcTopics.HIDE_NAV_PRINT_BTN, true);

  mainWindow!.on("close", () => removeFile());
};
