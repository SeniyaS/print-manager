import log from "electron-log";
import cryptoJs from "crypto-js";
import crypto from "crypto";
import { encode as urlSafeBase64Enc, isUrlSafeBase64 } from "url-safe-base64";
export const encrypt = (): string => {
  let arrBuff = new ArrayBuffer(256 / 8);
  const buf = Buffer.from(arrBuff);
  // let newBuff =Buffer.from(new ArrayBuffer(24));
  const digestEmp = crypto.createHash("md5").update(buf).digest();
  let newBuff = [...digestEmp];
  for (let i = 0, k = 16; i < 8; ) {
    newBuff[k++] = newBuff[i++];
  }
  log.info(newBuff);
  let VI = Buffer.alloc(8);
  let chiper = crypto.createCipheriv(
    "aes-192-ccm",
    Buffer.from("hello world"),
    VI
  );
  log.info(chiper);

  let encryptedArr = [chiper.update(Buffer.from(newBuff))];
  encryptedArr.push(chiper.final());
  let encrypted = Buffer.concat(encryptedArr);
  return encrypted.toString("utf-8");
};

export const encryptNew = (
  inputkey: string,
  keyformat: any,
  password: string,
  passwordformat: any
): string => {
  let shortkey = Buffer.from(inputkey, keyformat);
  let key = Buffer.alloc(24);
  key.fill("\0");
  for (let i = 0; i < shortkey.length; i++) {
    key[i] = shortkey[i];
  }
  let IV = Buffer.alloc(8);

  var expansionStart = shortkey.length > 16 ? shortkey.length : 16;
  for (let i = expansionStart; i < 24; i++) {
    key[i] = key[i - expansionStart];
  }
  console.log(key);
  const cipher = crypto.createCipheriv(
    "des-ede3-cbc",
    key.toString("utf-8"),
    IV
  );
  let passwordBuff = Buffer.from(password, passwordformat);

  let encryptedArr = [cipher.update(passwordBuff)];
  encryptedArr.push(cipher.final());
  let encrypted = Buffer.concat(encryptedArr);
  return encrypted.toString("utf-8");
};

/**
 *
 * @param rawtext raw text
 * @returns a base64 encoded,url safe chiper text
 */
export const encryptCryptoJS = (rawtext: string): string => {
  let key = cryptoJs.enc.Base64.parse("cLyPS3KoaSFGi/joRB3OUXC8j0tyqGkh");
  // log.info(key.toString());
  let iv = cryptoJs.enc.Base64.parse("DCI4TlpXQSs=");

  const enc = cryptoJs.TripleDES.encrypt(rawtext, key, {
    iv,
    mode: cryptoJs.mode.CBC,
    padding: cryptoJs.pad.Pkcs7,
  });

  const baseResult = enc.ciphertext.toString(cryptoJs.enc.Base64);
  log.info("cryptoJs :nonUrlSafe ", baseResult);
  // log.info(
  //   "cryptoJs ",
  //   isUrlSafeBase64(baseResult),
  //   urlSafeBase64Enc(baseResult)
  // );
  log.info("jsEncode  ", encodeURIComponent(baseResult));
  return isUrlSafeBase64(baseResult)
    ? baseResult
    : urlSafeBase64Enc(baseResult);
};
