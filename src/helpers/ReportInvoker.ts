import { encryptCryptoJS } from "./Chiper";
import log from "electron-log";
import JasperConnection from "./JasperConnection";
import moment from "moment";
export class JasperReport {
  private reportName: string = "";
  private appendString: string = "";
  private loggedUser: string = "";

  private serverIp: string;
  private dbHost1: string;
  private dbHost2: string;
  private dbPort: string;
  private dbName: string;
  private orgId: string;
  private dbUname: string;
  private dbPwd: string;
  private outputFormat: string; // outputFormat added

  private isCommercial: boolean = false;

  /**
   * @param serverIp     - report server IP
   * @param dbHost1      - host 1 for jdbc url
   * @param dbHost2      - host 2 for jdbc url
   * @param dbPort       - port for jdbc url
   * @param dbName       - service name for jdbc url
   * @param orgId        - tenant ID
   * @param dbUname      - username for the jdbc connection
   * @param dbPwd        - password for the jdbc connection
   * @param isCommercial - flag to identify whether the current report is running
   *                     on Commercial or Community
   */
  public constructor(
    serverIp: string,
    dbHost1: string,
    dbHost2: string,
    dbPort: string,
    dbName: string,
    orgId: string,
    dbUname: string,
    dbPwd: string,
    isCommercial: boolean,
    outputFormat: string
  ) {
    this.serverIp = serverIp;
    this.dbHost1 = dbHost1;
    this.dbHost2 = dbHost2;
    this.dbPort = dbPort;
    this.dbName = dbName;
    this.orgId = orgId;
    this.dbUname = dbUname;
    this.dbPwd = dbPwd;

    this.isCommercial = isCommercial;
    this.outputFormat = outputFormat;
  }

  /**
   * @param rptName name of the report
   * @param params  a hashmap of parameters
   * @param uName   username of the logged in user
   * @return URL for calling the report
   * @throws MalformedURLException
   * @throws IOException
   */
  public callReport(rptName: string, params: {}, uName: string): string {
    this.reportName = rptName;
    this.loggedUser = uName;

    const keys: string[] = this.userParamKeyArray(params);
    const vals: string[] = this.userParamValArr(keys, params);

    return this.runReport(keys, vals);
  }

  /**
   * @param keys an array of parameter names
   * @param vals an array of parameter values
   * @return URL string for calling the report
   * @throws MalformedURLException
   * @throws IOException
   */
  private runReport(keys: string[], vals: string[]): string {
    let con: JasperConnection = new JasperConnection(
      this.isCommercial,
      this.orgId,
      this.outputFormat
    );

    const commonReportPath: string = this.serverIp + "/" + con.getReportURL();
    con.setReportURL(commonReportPath + this.crateReportURL(keys, vals));

    return con.getReportURL();
  }

  /**
   * @param paramMap object of parameters
   * @return an array of parameter names
   */
  private userParamKeyArray(paramMap: Object): string[] {
    let keys: string[] = Object.keys(paramMap);

    return keys;
    // Set<String> set = paramMap.keySet();
    // String strSet = set.toString();
    // strSet = strSet.replaceAll("([\\[\\]\\s])", "");
    // return strSet.split("([\\,])");
  }

  /**
   * @param keyArr   parameter name array
   * @param paramMap hashmap of parameters
   * @return an array of parameter values
   */
  private userParamValArr(keyArr: string[], paramMap: any): string[] {
    let valArr: string[] = [];
    for (let i = 0; i < keyArr.length; i++) {
      valArr.push(paramMap[keyArr[i]]);
    }
    return valArr;
  }

  /**
   * @param keys report parameter name array
   * @param vals report parameter value array
   * @return appended string of parameters
   */
  private crateReportURL(keys: string[], vals: string[]): string {
    this.appendString = this.reportName;

    // StringBuilder sb = new StringBuilder();
    let sb: string[] = [];

    for (let i = 0; i < keys.length; i++) {
      sb.push("&");
      sb.push(keys[i]);
      sb.push("=");
      sb.push(vals[i]);
    }

    // appending Principal Parameter to the report URL itself
    sb.push("&");
    sb.push("pp=");
    sb.push(this.createLoginToken());

    return this.appendString + sb.join("");
  }

  /**
   * @return token for logging
   */
  private createLoginToken(): string {
    const expTime: string | null = this.getExpireTime();

    let token: string = "";

    if (this.isCommercial) {
      token =
        "u=" +
        this.loggedUser +
        "|" +
        "o=" +
        this.orgId +
        "|" +
        "r=EXT_" +
        this.orgId.toUpperCase() +
        "_USER,ROLE_USER|" +
        "exp=" +
        expTime +
        "|" +
        "dbHost1=" +
        this.dbHost1 +
        "|" +
        "dbHost2=" +
        this.dbHost2 +
        "|" +
        "dbPort=" +
        this.dbPort +
        "|" +
        "dbUname=" +
        this.dbUname +
        "|" +
        "dbPwd=" +
        this.dbPwd +
        "|" +
        "dbName=" +
        this.dbName;
    } else {
      token = "u=" + this.loggedUser + "|exp=" + expTime;
    }

    log.info("token", token);

    // return ciper.encrypt(token).replaceAll("\\p{C}", "?");
    //TODO need to check this replaceAll
    return encryptCryptoJS(token).replace("\\p{C}", "?");
  }

  /**
   * @return expire time as a string
   */
  private getExpireTime(): string | null {
    try {
      // let date:Date = new Date();
      // log.info("now time:",date);
      // date.setSeconds(date.getSeconds()+120);
      // log.info("exp time:",date);
      let now = moment();
      //taking the system default time zone
      log.info("now time:", now);
      now.seconds(now.seconds() + 120);

      log.info("exp time:", now.format("yyyyMMddHHmmssZ"));

      // log.info(new Date());
      // Calendar calendar = Calendar.getInstance();
      // calendar.add(Calendar.SECOND, 120);
      // Date expDate = calendar.getTime();

      // // System.out.println(expDate);

      // SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmssZ");
      // SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
      // System.out.println(df.format(expDate)+" token timeout in 2 minutes");
      return now.format("yyyyMMddHHmmssZ");
    } catch (e) {
      e.printStackTrace();
      return null;
    }
  }

  /**
   * @param user logged in user
   * @return URL for logging
   */
  public getLoginUrl(user: string): string {
    this.loggedUser = user;
    const con: JasperConnection = new JasperConnection(
      this.isCommercial,
      this.orgId,
      this.outputFormat
    );

    let tempLoginUrl: string = this.serverIp + "/" + con.getLoginURL();
    // con.setLoginURL(con.getLoginURL() + createLoginToken());
    return tempLoginUrl + this.createLoginToken();
  }

  /**
   * Test Client
   */
  public static test() {
    const serverIp = "http://lolcdevjbossjas:8080";
    // String serverIp = "http://localhost:8080";

    // JasperReport jr = new JasperReport(serverIp , "lolcdc1-scan.lolc.com" ,
    // "temp" , "1536" , "fusionp" , "lofcfusion" , "rep005" , "rep321" , true ,
    // "pdf");
    const jr: JasperReport = new JasperReport(
      serverIp,
      "10.254.10.38",
      "10.254.10.38",
      "1528",
      "fusdev",
      "lofcfusion",
      "SURE",
      "sure3",
      true,
      "pdf"
    );
    // JasperReport jr = new JasperReport(serverIp , dbHost1 , dbHost2 , dbPort ,
    // dbName , orgId , dbUname , dbPwd , isCommercial);
    // HashMap<String, String> hm = new HashMap<String, String>();

    // hm.put("P_REGAT", "04-07-2017");
    // hm.put("P_NOTTYP", "3");
    // hm.put("P_DUE", "20170704");
    // hm.put("P_DATE", "20170704");
    // hm.put("P_USER", "SURE");
    // hm.put("P_SCM", "D_005_S");
    const hm = {
      P_REGAT: "04-07-2017",
      P_NOTTYP: "3",
      P_DUE: "20170704",
      P_DATE: "20170704",
      P_USER: "SURE",
      P_SCM: "D_005_S",
    };
    // hm.put("", "");
    // let a: string = null;

    try {
      // jr.callReport("report1", hm , "RanjithK");
      // System.out.println(jr.getLoginUrl("SURE"));
      log.info("login Url:", jr.getLoginUrl("SURE"));
      log.info(jr.callReport("rpt_dad_mail", hm, "SURE"));
      // MyCipher cip = new MyCipher();
      // System.out.println("decrypt " + cip.decrypt(
      // 		"P-C0zAGLd5MqZk3we4jbsSq1KHJESl2XAAP0H6Gg7JmgtNj6_a265rfTs2b08nmzja6rpq3LaTDj?lRQPuC4ykt3rsHp-JnPck3GLr3EJm6pBrGopxLrdBEIUlwosvMttHVP-bSW08v2PMHLffgIg-PDt?PNX9y8ZxuEfUt_ZvKOPkXYHMtyoae3BScEKeFsn1iQ7I0lsb2p5YY_HI0Mj7FCAGDMGWw07WDhrh?HNxzX6sPdlVeo5GJJQ==?"));
    } catch (e) {
      e.printStackTrace();
    }
  }
}
