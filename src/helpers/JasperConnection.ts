import log, { info } from "electron-log";

class JasperConnection {
  private communityLoginURL: string = "jasperserver/?pp=";
  private communityReportURL: string =
    "jasperserver/flow.html?_flowId=viewReportFlow&standAlone=true&_flowId=viewReportFlow&ParentFolderUri=%2Freports&decorate=no&reportUnit=%2Freports%2F";

  private commercialLoginURL: string = "jasperserver-pro/?pp=";
  private commercialReportURL: string =
    "jasperserver-pro/flow.html?_flowId=viewReportFlow&standAlone=true&_flowId=viewReportFlow&decorate=no~~~&ParentFolderUri=%2Forganizations%2F***%2Freports&reportUnit=%2Forganizations%2F***%2Freports%2F";

  /**
   *
   * @param isCommercial
   * @param orgId
   * @param outputFormat
   */
  public constructor(
    private isCommercial: boolean,
    private orgId: string,
    private outputFormat: string
  ) {}

  /**
   * @returns loginUrl
   */
  public getLoginURL(): string {
    return this.isCommercial ? this.commercialLoginURL : this.communityLoginURL;
  }

  /**
   * @param loginURL
   */
  public setLoginURL(loginURL: string): void {
    if (this.isCommercial) {
      this.commercialLoginURL = loginURL;
    } else {
      this.communityLoginURL = loginURL;
    }
  }

  /**
   * @return reportURL
   */
  public getReportURL(): string {
    if (this.isCommercial) {
      let tempUrl: string = "";

      if (
        this.outputFormat == null ||
        this.outputFormat == "" ||
        this.outputFormat === "N"
      ) {
        //TODO check the regEX \\~\\~\\~
        //removed escape characters by SeniyaS
        // tempUrl = this.commercialReportURL.replace("\\~\\~\\~", "");
        // return tempUrl.replace("\\*\\*\\*", this.orgId);
        tempUrl = this.commercialReportURL.replace("~~~", "");
        return tempUrl.replace("***", this.orgId);
      } else {
        info("getReportURL/tempUrl" + tempUrl);

        //changed regREX : \\~\\~\\~ -> ~~~ by SeniyaS
        tempUrl = this.commercialReportURL.replace(
          "~~~",
          "&output=" + this.outputFormat
        );
        //removed escape characters by SeniyaS
        return tempUrl.replace("***", this.orgId);
        // return tempUrl.replace("\\*\\*\\*", this.orgId);
      }

      //return reportURLCommercial.replaceAll("\\*\\*\\*", orgId);
    } else {
      return this.communityReportURL;
    }
  }

  /**
   * @param reportURL
   */
  public setReportURL(reportURL: string): void {
    if (this.isCommercial) {
      this.commercialReportURL = reportURL;
    } else {
      this.communityReportURL = reportURL;
    }
  }
}

export default JasperConnection;
