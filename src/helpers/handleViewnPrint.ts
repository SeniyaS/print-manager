import { BrowserWindow, ipcMain, IpcMainEvent } from "electron";
import log from "electron-log";
import { ipcTopics } from "../configs/ipcConfigs";
import { downloadPDF } from "../services/pdfDownload";
import {
  getPrintersList,
  printWithOptions,
} from "../services/printService.electron";

export const handleViewAndPrint = async (pdfUrl: string) => {
  const [docUri, removeFile] = await downloadPDF(pdfUrl);

  const mainWindow = BrowserWindow.getFocusedWindow();
  const printO = (e: IpcMainEvent, printer: string) => {
    log.info("printO", printer);
    printWithOptions(docUri, printer);
    // removeFile()
  };

  ipcMain.on(ipcTopics.PRINT_WITH_OPTIONS, printO);

  mainWindow!.webContents.send(ipcTopics.GET_PRINTERS, await getPrintersList());
  mainWindow!.webContents.send(ipcTopics.CHANGE_PDF_URI, docUri);
  mainWindow!.webContents.send(ipcTopics.HIDE_NAV_PRINT_BTN, false);
  mainWindow!.on("close", () => {
    removeFile();
  });
};
