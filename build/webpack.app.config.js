const path = require("path");
const merge = require("webpack-merge");
const base = require("./webpack.base.config");
module.exports = (env) => {
  return merge(base(env), {
    entry: {
      background: path.resolve(__dirname, "../src/main/background"),
      app: path.resolve(__dirname, "../src/renderer/app"),
    },
    module: {
      rules: [
        {
          test: /\.ts$/,
          use: ["ts-loader"],
          exclude: [/node_modules/],
        },
      ],
    },

    output: {
      filename: "[name].js",
      path: path.resolve(__dirname, "../app"),
    },
  });
};
