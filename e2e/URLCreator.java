package e2e;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class URLCreator {

    public static void main(String[] args) {
        String url = "http://lolcdevjbossjas:8080/jasperserver-pro/flow.html?_flowId=viewReportFlow&standAlone=true&_flowId=viewReportFlow&decorate=no&output=pdf&ParentFolderUri=%2Forganizations%2Flofcfusion%2Freports&reportUnit=%2Forganizations%2Flofcfusion%2Freports%2Frpt_dad_mail&P_NOTTYP=3&P_REGAT=04-07-2017&P_DUE=20170704&P_SCM=D_005_S&P_DATE=20170704&P_USER=SURE&pp=P-C0zAGLd5MwzjuNo_hVcQhzgfRmGgMXBvJH83QKNhleXPRMJlOlquBCwBmrNwHPpMvNhvk7nqQI?ZUYCgcSsbO3L9tVcDoz7J6YX_7EfYqmWrelkZTJwUzyqWu71Us01LXMS4ig2i4N7PJcDPv47eAEa?M7keOiBVZtQ3EwISSylABI5fU1lyCs0O3aTZEQIjbz19iIlAXbQQft9fuvg3IMzOhppKAd23oct2?neT0YPI=";
        String newUrl = "hansen://gsfgg?msg=" + URLEncoder.encode(url, StandardCharsets.UTF_8);
        System.out.println(newUrl);
    }
}